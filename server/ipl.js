/* reference output
{
  'Sunrisers Hyderabad': 17,
  'Royal Challengers Bangalore': 35,
  'Mumbai Indians': 48,
  'Rising Pune Supergiant': 5,
  'Gujarat Lions': 10,
  'Kolkata Knight Riders': 44,
  'Kings XI Punjab': 28,
  'Delhi Daredevils': 33,
  'Chennai Super Kings': 42,
  'Rajasthan Royals': 34,
  'Deccan Chargers': 19,
  'Kochi Tuskers Kerala': 4,
  'Pune Warriors': 3,
  'Rising Pune Supergiants': 3
}
 */
function numberTeamTossMatchWon(arrMatchesObj, arrDeliveriesObj) {
    let teams = [];
    let resultObj = {};

    for (let match of arrMatchesObj) {
        teams.push(match.team1);
        teams.push(match.team2);
    }
    teams = Array.from(new Set(teams));

    for (let team of teams) {
        for (let match of arrMatchesObj) {
            if (team == match.toss_winner && team == match.winner) {
                if (resultObj[team]) {
                    resultObj[team]++;
                } else {
                    resultObj[team] = 1;
                }
            }
        }
    }
    return resultObj;

}
/* reference output
{
  '2008': [ 'SE Marsh', 5 ],
  '2009': [ 'YK Pathan', 3 ],
  '2010': [ 'SR Tendulkar', 4 ],
  '2011': [ 'CH Gayle', 6 ],
  '2012': [ 'CH Gayle', 5 ],
  '2013': [ 'MEK Hussey', 5 ],
  '2014': [ 'GJ Maxwell', 4 ],
  '2015': [ 'DA Warner', 4 ],
  '2016': [ 'V Kohli', 5 ],
  '2017': [ 'BA Stokes', 3 ]
}

*/

function playerHighestPlayerOfMatchPerSeason(arrMatchesObj, arrDeliveriesObj) {
    let interMediateObj = {}

    for (let match of arrMatchesObj) {
        if (interMediateObj[match.season]) {
            interMediateObj[match.season].push(match.player_of_match);
        } else {
            interMediateObj[match.season] = [match.player_of_match];
        }
    }

    let finalObj = {};

    for (let year in interMediateObj) {
        let innerObj = interMediateObj[year].reduce((accVal, currVal) => {
            if (accVal[currVal]) {
                accVal[currVal]++;
                return accVal;
            } else {
                accVal[currVal] = 1;
                return accVal;
            }
        }, {})
        let sortedArr = Object.entries(innerObj).sort((a, b) => (b[1] - a[1]));
        finalObj[year] = sortedArr[0];
    }

    return finalObj;
}

/* Reference output

dateStructure 
[
  [ '2008', [ '60', '117' ] ],
  [ '2009', [ '118', '174' ] ],
  [ '2010', [ '175', '234' ] ],
  [ '2011', [ '235', '307' ] ],
  [ '2012', [ '308', '381' ] ],
  [ '2013', [ '382', '457' ] ],
  [ '2014', [ '458', '517' ] ],
  [ '2015', [ '518', '576' ] ],
  [ '2016', [ '577', '636' ] ],
  [ '2017', [ '1', '59' ] ]
]

final Output :
{
  '2008': 98,
  '2009': 109,
  '2010': 142,
  '2011': 118,
  '2012': 109,
  '2013': 136,
  '2014': 119,
  '2015': 128,
  '2016': 149,
  '2017': 120
}

*/

// calculating strike rate of Virat Kohli across all seasons since Name not mentioned  

function batsmanStrikeRatePerSeason(arrMatchesObj, arrDeliveriesObj) {
    let seasonMatchIDObj = {};
    for (let match of arrMatchesObj) {
        if (seasonMatchIDObj[match.season]) {
            seasonMatchIDObj[match.season][1] = match.id;
        } else {
            seasonMatchIDObj[match.season] = [match.id, 0];
        }

    }

    let dateStructure = Object.entries(seasonMatchIDObj);

    let finalObj = {};

    for (let i = 0; i < dateStructure.length; i++) {
        for (let delObj of arrDeliveriesObj) {
            if (delObj.batsman == 'V Kohli' && (Number(delObj.match_id) <= Number(dateStructure[i][1][1]) && Number(delObj.match_id) >= Number(dateStructure[i][1][0]))) {
                if (finalObj[dateStructure[i][0]]) {
                    finalObj[dateStructure[i][0]].runs.push(Number(delObj.batsman_runs));
                    finalObj[dateStructure[i][0]].balls++;
                } else {
                    finalObj[dateStructure[i][0]] = {
                        runs: [],
                        balls: 1
                    };
                    finalObj[dateStructure[i][0]].runs.push(Number(delObj.batsman_runs));
                }
            }

        }
    }

    let finalReturnObject = {};

    for (let year in finalObj) {
        finalReturnObject[year] = Math.round(((finalObj[year].runs.reduce((x, y) => (x + y)) / finalObj[year].balls) * 100));
    }

    return finalReturnObject;
}

/* Reference output:
{   .
    .
    .
    .
'M Vijay': [
    'R Bhatia',        'HH Gibbs',        'SK Trivedi',      'RV Uthappa',
    'WPUJC Vaas',      'LR Shukla',       'J Theron',        'EJG Morgan',
    'YK Pathan',       'MK Pandey',       'FY Fazal',        'Harbhajan Singh',
    'RP Singh',        'KD Karthik',      'KC Sangakkara',   'RJ Harris',
    'SS Tiwary',       'R Bhatia',        'AC Gilchrist',    'Z Khan',
    'NLTC Perera',     'RG Sharma',       'NL McCullum',     'M Manhas',
    'PP Ojha',         'J Botha',         'Iqbal Abdulla',   'J Botha',
    'Y Venugopal Rao', 'RP Singh',        'AB de Villiers',  'S Aravind',
    'DL Vettori',      'Harbhajan Singh', 'Ankit Sharma',    'IK Pathan',
    'M Muralitharan',  'AB Dinda',        'A Mishra',        'RP Singh',
    'SR Watson',       'SP Narine',       'AC Gilchrist',    'RG Sharma',
    'DA Warner',       'Shakib Al Hasan', 'MM Patel',        'KB Arun Karthik',
    'LRPL Taylor',     'M Morkel',        'SMSM Senanayake', 'A Chandila',
    'Q de Kock',       'MG Johnson',      'PA Patel',        'DH Yagnik',
    'BJ Rohrer',       'CH Gayle',        'DR Smith',        'RG Sharma',
    'YS Chahal',       'MK Pandey',       'F du Plessis',    'DW Steyn',
    'RG Sharma',       'SR Watson',       'F du Plessis',    'SP Narine',
    'M Muralitharan',  'BE Hendricks',    'AP Tare',         'SV Samson',
    'J Suchith',       'JP Duminy',       'AD Russell',      'R Tewatia',
    'DJ Bravo',        'GH Vihari',       'UBT Chand',       'HV Patel',
    'SP Narine',       'S Dhawan',        'RA Jadeja',       'Z Khan',
    'MS Dhoni',        'PP Chawla',       'NV Ojha',         'JC Buttler',
    'DJ Bravo',        'Shakib Al Hasan', 'CR Brathwaite',   'YS Chahal',
    'DA Warner',       'S Aravind',       'R Ashwin'
  ],
'J Yadav': [ 'SS Tiwary', 'YS Chahal' ],
  'UT Khawaja': [
    'SV Samson',
    'SR Watson',
    'DJ Hooda',
    'SA Yadav',
    'SS Iyer',
    'DA Miller'
  ],
  'S Kaushik': [ 'S Aravind' ],
  'F Behardien': [ 'AB de Villiers', 'AM Rahane' ],
  'KJ Abbott': [ 'AB de Villiers' ],
  'ER Dwivedi': [ 'V Kohli', 'TA Boult' ]
}

*/



function highestNumberPlayerDismissed(arrMatchesObj, arrDeliveriesObj) {
    let dataArray = [];
    for (let delObj of arrDeliveriesObj) {
        if (delObj.dismissal_kind == 'caught' || delObj.dismissal_kind == 'run out' || delObj.dismissal_kind == 'stumped') {
            dataArray.push([delObj.player_dismissed, delObj.fielder])
        } else if (delObj.dismissal_kind == 'bowled' || delObj.dismissal_kind == 'lbw' || delObj.dismissal_kind == 'caught and bowled' || delObj.dismissal_kind == 'retired hurt' || delObj.dismissal_kind == 'hit wicket') {
            dataArray.push([delObj.player_dismissed, delObj.bowler]);
        }
    }

    let uniqueDismissedPlayers = [];
    for (let arr of dataArray) {
        uniqueDismissedPlayers.push(arr[0]);
    }
    uniqueDismissedPlayers = Array.from(new Set(uniqueDismissedPlayers));

    let resultObj = {};
    for (let player of uniqueDismissedPlayers) {
        for (let arr of dataArray) {
            if (player == arr[0]) {
                if (resultObj[player]) {
                    resultObj[player].push(arr[1]);
                } else {
                    resultObj[player] = [arr[1]];
                }
            }
        }
    }


    return resultObj;
}

// Overs are written as decimals from 0.1 to 0.6, 
// so must be converted into actual decimals (e.g. 0.3 Overs is actually 0.5 as 3 is half of 6) 
// before used in the calculation. For example, a bowler conceding 31 runs from 10.2 overs 
// (i.e. 10 overs and 2 balls), has an economy rate of 31/10.33333 = 3.0 runs per over.

/* Output for reference ::
{
  'JP Faulkner': {
    runs: [
      1, 4, 6, 0, 0,
      0, 1, 1, 6, 1,
      1
    ],
    balls: 11
  },
  'JJ Bumrah': { runs: [
      1, 0, 1, 0,
      0, 0, 1, 1
    ], balls: 8 },
  'Kamran Khan': { runs: [
      1, 1, 1, 4,
      4, 4, 0
    ], balls: 7 },
  'BAW Mendis': { runs: [ 6, 2, 6, 4 ], balls: 4 },
  'J Theron': { runs: [ 1, 0, 2, 6, 0 ], balls: 5 },
  'M Muralitharan': { runs: [ 6, 0, 0, 4 ], balls: 4 },
  'R Vinay Kumar': { runs: [
      2, 1, 1, 6,
      2, 6, 2
    ], balls: 7 },
  'DW Steyn': { runs: [ 2, 1, 4, 1, 6, 1 ], balls: 6 },
  'UT Yadav': { runs: [ 1, 1, 1, 0, 6, 6 ], balls: 6 },
  'R Rampaul': { runs: [ 0, 4, 0, 6, 1, 0 ], balls: 6 },
  'SP Narine': { runs: [ 1, 2, 1, 4, 1, 2 ], balls: 6 },
  'CH Morris': { runs: [
      0, 1, 5, 4,
      4, 0, 0
    ], balls: 7 },
  'MG Johnson': { runs: [ 0, 5, 1, 0 ], balls: 4 }
}


*/


function bowlerBestEcoInSuperOvers(arrMatchesObj, arrDeliveriesObj) {
    let superOverArray = [];
    for (let delObj of arrDeliveriesObj) {
        if (delObj[`is_super_over`] != 0) {
            superOverArray.push(delObj);
        }
    }

    let resultObj = {}
    for (let superOverObj of superOverArray) {
        let {
            bowler,
            total_runs,
            bye_runs,
            legbye_runs
        } = superOverObj;
        if (resultObj[bowler]) {
            resultObj[bowler].runs.push(total_runs - bye_runs - legbye_runs);
            resultObj[bowler].balls++;
        } else {
            resultObj[bowler] = {
                runs: [],
                balls: 1
            }
            resultObj[bowler].runs.push(total_runs - bye_runs - legbye_runs)
        }
    }

    return resultObj;
}




module.exports = {
    numberTeamTossMatchWon,
    playerHighestPlayerOfMatchPerSeason,
    batsmanStrikeRatePerSeason,
    highestNumberPlayerDismissed,
    bowlerBestEcoInSuperOvers
};