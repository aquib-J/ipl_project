# Questions

### 1.Find the number of times each team won the toss and also won the match

### 2.Find a player who has won the highest number of Player of the Match awards for each season

### 3.Find the strike rate of a batsman for each season

### 4.Find the highest number of times one player has been dismissed by another player

### 5.Find the bowler with the best economy in super overs


----------------------------------------------------------------------------------------------
## Commands to run locally :: 

    * npm run ipl    ==> Prepares the data for consumption

    * npm run start  ==> Runs on localhost:8080  