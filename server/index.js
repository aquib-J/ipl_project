const csv = require('csvtojson');
const fs = require('fs');

const {
    numberTeamTossMatchWon,
    playerHighestPlayerOfMatchPerSeason,
    batsmanStrikeRatePerSeason,
    highestNumberPlayerDismissed,
    bowlerBestEcoInSuperOvers
} = require('./ipl');

const deliveriesDataFilePath = '../data/deliveries.csv';
const mathchesDataFilePath = '../data/matches.csv';

const JSON_OUTPUT_FILE_PATH = '../public/output/data.json';

async function main() {

    try {

        const jsonArrayMatches = await csv().fromFile(mathchesDataFilePath);
        const jsonArrayDeliveries = await csv().fromFile(deliveriesDataFilePath);

        let result1 = numberTeamTossMatchWon(jsonArrayMatches, jsonArrayDeliveries);
        let result2 = playerHighestPlayerOfMatchPerSeason(jsonArrayMatches, jsonArrayDeliveries);
        let result3 = batsmanStrikeRatePerSeason(jsonArrayMatches, jsonArrayDeliveries);
        let result4 = highestNumberPlayerDismissed(jsonArrayMatches, jsonArrayDeliveries);
        let result5 = bowlerBestEcoInSuperOvers(jsonArrayMatches, jsonArrayDeliveries);


        saveAll(result1, result2, result3, result4, result5);



    } catch (e) {
        console.error(e);
    }
}

function saveAll(result1, result2, result3, result4, result5) {
    const jsonData = {
        numberTeamTossMatchWon: result1,
        playerHighestPlayerOfMatchPerSeason: result2,
        batsmanStrikeRatePerSeason: result3,
        highestNumberPlayerDismissed: result4,
        bowlerBestEcoInSuperOvers: result5
    }
    const jsonDataString = JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonDataString, 'utf-8', err => {
        if (err) {
            console.error(err);
        }
    })
}

main();